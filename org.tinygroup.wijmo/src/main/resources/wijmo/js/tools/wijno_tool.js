function cellFormatter_checkBox(args, gridId, checkBoxClass) {
	if (args.row.type & $.wijmo.wijgrid.rowType.data) {
		args.$container
				.empty()
				.append(
						$(
								"<input type='checkbox' class='"
										+ checkBoxClass + "' />")
								.click(function(e) {
									e.stopPropagation();
								})
								.change(
										function(e) {
											// add the row if checkbox is
											// selected
											if (e.target.checked) {
												$("#" + gridId).wijgrid(
														"selection").addRows(
														args.row.dataRowIndex);
											} else {
												$("#" + gridId)
														.wijgrid("selection")
														.removeRows(
																args.row.dataRowIndex);
											}
										}));
		return true;
	}
}

function cellFormatter_img(args, imgPath, id) {
	if (args.row.type & wijmo.grid.rowType.data) {
		args.$container.append($("<img />").attr("src", imgPath).attr(
				"onclick", "modify(" + id + ")"));
		return true;
	}
}

function cellFormatter_href(args, url, value) {
	if (args.row.type & wijmo.grid.rowType.data) {
		args.$container.append("<a href=\"" + url + "\">" + value + "</a>");
		return true;
	}
}
